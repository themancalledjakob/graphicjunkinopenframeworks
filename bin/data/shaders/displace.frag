uniform sampler2D colormap;
//uniform sampler2D bumpmap;
varying vec2  TexCoord;

void main(void) {
//    float midColor;
    
	gl_FragColor = texture2D(colormap, TexCoord);
//    midColor = (gl_FragColor.r + gl_FragColor.g + gl_FragColor.b)*0.3;
//    gl_FragColor.r = gl_FragColor.r;
//    gl_FragColor.g = gl_FragColor.g;
//    gl_FragColor.b = gl_FragColor.b;
}