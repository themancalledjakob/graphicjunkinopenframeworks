#include "testApp.h"

#define STRINGIFY(A) #A


void addRectangleToMesh(ofVboMesh * mesh, ofVec3f v1, ofVec3f v2, ofVec3f v3, ofVec3f v4, int fX, int fY, int index, ofVec3f box) {
    
    /*
     the amazing faces and texture coordinates
     
     (0,0)   (0.2,0)      (0.5,0)     (0.7,0)
     e----------f------------h----------g------------e (1,0)
     |          '            '          '            |
     |          '            '          '            |
     |    r0    '     r1     '    r2    '     r3     |
     |          '            '          '            |
     |          '            '          '            |
     a----------b------------d----------c------------a  (1,1)
     (0,1)   (0.2,1)     (0.5,1)     (0.7,1)
     
        box.x       box.y      box.x      box.y
     
     |---------------------- r ----------------------|
                   ( 2*box.x + 2*box.y )
     
     r0.left = 0
     r0.right = box.x / r

     r1.left = r0.right
     r1.right = 0.5
     
     r2.left = 0.5
     r2.right = 0.5 + r0.right
     
     r3.left = 0.5 + r0.right
     r3.right = 1
     
     and everything must be multiplied by fX ( width of our fbo ) for texture coordinates
     
     */
    
    float left = -1;
    float right = -1;
    float r = 2*box.x + 2*box.y;
    float r0 = box.x / r;
    
    switch(index){
        case 0:
            left = 0.0;
            right = r0*fX;
            cout << "left " << index << ": " << left << endl;
            cout << "right " << index << ": " << right << endl;
            break;
        case 1:
            left = r0*fX;
            right = 0.5*fX;
            cout << "left " << index << ": " << left << endl;
            cout << "right " << index << ": " << right << endl;
            break;
        case 2:
            left = 0.5*fX;
            right = (0.5+r0)*fX;
            break;
        case 3:
            left = (0.5+r0)*fX;
            right = fX;
            cout << "left " << index << ": " << left << endl;
            cout << "right " << index << ": " << right << endl;
            break;
        default:
            cout << "ERRORRRR in addRectangleToMesh. default is not a valuable option" << endl;
            break;
    }
    
    float top = 1*fY;
    float bottom = (0*fY);
    
    mesh->addVertex(v1);
    mesh->addTexCoord(ofVec2f(left,     bottom));
    mesh->addVertex(v2);
    mesh->addTexCoord(ofVec2f(right,    bottom));
    mesh->addVertex(v3);
    mesh->addTexCoord(ofVec2f(left,     top));
    
    mesh->addVertex(v4);
    mesh->addTexCoord(ofVec2f(right,    top));
    mesh->addVertex(v3);
    mesh->addTexCoord(ofVec2f(left,     top));
    mesh->addVertex(v2);
    mesh->addTexCoord(ofVec2f(right,    bottom));
    
}

//--------------------------------------------------------------
void testApp::setup(){
    
    ofBackground(0);
	ofSetFrameRate(40);
    
    Mainfbo.allocate(1280, 240, GL_RGB);
    superFbo.allocate(ofGetWindowWidth(), ofGetWindowHeight(),GL_RGBA);
    
	roomHeight = 200;
    roomWidth = 400;
	roomDepth = 600;
    
    fboSize = ofVec2f(ofGetScreenWidth(),ofGetScreenHeight());
    
    makeMesh(roomHeight,roomWidth,roomDepth);
    
    
    // this is the shader to wrap the MainFbo around the walls of the room
//    string shaderProgram = STRINGIFY(
//                                     uniform sampler2DRect tex0;
//                                     
//                                     void main (void){
//                                         vec2 pos = gl_TexCoord[0].st;
//                                         gl_FragColor = texture2DRect(tex0, pos);
//                                     }
//                                     );

    
    string shaderProgram = STRINGIFY(
                                     uniform sampler2DRect tex0;
                                     uniform sampler2DRect maskTex;
//                                     uniform float time;
//                                     uniform float limit;
                                     
                                     void main (void){
                                         vec2 pos = gl_TexCoord[0].st;
                                         
//                                         vec2 final_pos;
//                                         vec2 final_pos_reverse;
//                                         
//                                         final_pos.x = (pos.x + time);
//                                         if(final_pos.x > limit) {
//                                             final_pos.x = pos.x - limit + time;
//                                         }
//                                         
//                                         final_pos_reverse.x = (pos.x - time);
//                                         if(final_pos_reverse.x < 0.0) {
//                                             final_pos_reverse.x = pos.x + limit - time;
//                                         }
//                                         
//                                         final_pos.y = pos.y;
//                                         final_pos_reverse.y = pos.y;
                                         
//                                         vec4 maskTxt = texture2DRect(maskTex, pos);
                                         vec4 stillTxt = texture2DRect(tex0, pos);
//                                         vec4 movingReverseTxt = texture2DRect(tex0, final_pos_reverse);
                                         //moving
//                                         vec4 movingTxt = texture2DRect(tex0, final_pos);                                         float mask = (maskTxt.x + maskTxt.y + maskTxt.z);
//                                         if (mask < 0.4) {
//                                             gl_FragColor.xyzw = movingReverseTxt.xyzw;
//                                         } else {
//                                             gl_FragColor.xyzw = movingTxt.xyzw;
//                                         }
                                         gl_FragColor = stillTxt;
                                     }
                                     );
    
    shader.setupShaderFromSource(GL_FRAGMENT_SHADER, shaderProgram);
    shader.linkProgram();

    // this is a testing image
    lust.loadImage("frederic.png");
    
    gameCamera.setup();
    try {
        gameCamera.loadCameraPosition();
    } catch (exception e) {
        cout << "gameCamera.loadCameraPosition error" << endl;
    }
    
    mouseMover = true;
    
    fboLust.allocate(fboSize.x, fboSize.y, GL_RGB);
    
    ofSetBackgroundAuto(true);
    
    bufferGrid = ofVec2f(mouseX,mouseY);
    
    for (int i = 0; i < 10; i++){
        ofImage tempImage;
        tempImage.loadImage("102NIKON/image" + ofToString(i) + ".JPG");
//        tempImage.loadImage("102NIKON/image0.png");
        images.push_back(tempImage);
    }
    
    fixedObjects = true;
    
    
}

//--------------------------------------------------------------
void testApp::update(){
    
    fboLust.begin();
    drawLust();
    fboLust.end();
}

void testApp::makeMesh(float boxHeight, float boxWidth, float boxDepth){
    
    
    /*
     the amazing cube
     
     e------------f
     |\           '\
     | \          ' \
     |  g------------h     
     |  |         '  |        y
     |  |         '  |      z |
     a -| -  -  - b  |       \|
      \ |          \ |         ---x
       \|           \|
        c------------d        
     
     */
    
    // construct vertexes
    
    ofVec3f a(  0,        0,          0           );
    ofVec3f e(  0,        boxHeight,  0           );
    ofVec3f b(  boxWidth, 0,          0           );
    ofVec3f f(  boxWidth, boxHeight,  0           );
    ofVec3f c(  0,        0,          boxDepth    );
    ofVec3f g(  0,        boxHeight,  boxDepth    );
    ofVec3f d(  boxWidth, 0,          boxDepth    );
    ofVec3f h(  boxWidth, boxHeight,  boxDepth    );
    
    /*
     the amazing faces and texture coordinates
     
     (0,0)   (0.2,0)      (0.5,0)     (0.7,0)
     e----------f------------h----------g------------e (1,0)
     |          '            '          '            |
     |          '            '          '            |
     |    r0    '     r1     '    r2    '     r3     |
     |          '            '          '            |
     |          '            '          '            |
     a----------b------------d----------c------------a  (1,1)
     (0,1)   (0.2,1)     (0.5,1)     (0.7,1)
     
        width       depth      width      depth
     
     
     */
    
    
    int faktorX = Mainfbo.getWidth();
    int faktorY = Mainfbo.getHeight();
    
    addRectangleToMesh(&boxMesh, e, f, a, b, faktorX, faktorY, 0, ofVec3f(boxWidth, boxDepth, boxHeight));
    addRectangleToMesh(&boxMesh, f, h, b, d, faktorX, faktorY, 1, ofVec3f(boxWidth, boxDepth, boxHeight));
    addRectangleToMesh(&boxMesh, h, g, d, c, faktorX, faktorY, 2, ofVec3f(boxWidth, boxDepth, boxHeight));
    addRectangleToMesh(&boxMesh, g, e, c, a, faktorX, faktorY, 3, ofVec3f(boxWidth, boxDepth, boxHeight));
}





//--------------------------------------------------------------
void testApp::drawFbo(){
    
    ofFbo tempFbo;
    fbos.push_back(tempFbo);
    fbos[fbos.size()-1].allocate(fboSize.x, fboSize.y, GL_RGBA);
    fbos[fbos.size()-1].begin();
    fbos[fbos.size()-1].end();
    if (fbos.size() > 4) {
        fbos.erase(fbos.begin());
    }
}
//--------------------------------------------------------------
void testApp::drawLust(){
}

//--------------------------------------------------------------
void testApp::draw(){
    int floatingGrey = (1+sin(ofGetElapsedTimef()))*127.5;
    ofBackground(floatingGrey);

    
    /*
       0.2    0.3     0.2    0.3
      ______ _______ ______ _______
     |      |       |      |       |
     |   0  |   1   |   2  |   3   |
     |______|_______|______|_______|
     
     |<--------     1      ------->|
     
     0: 0.0 - 0.2
     1: 0.2 - 0.5
     2: 0.5 - 0.7
     3: 0.7 - 1.0
     
     */
    

    Mainfbo.begin();
    if (fbos.size() > 3){
//        ofFbo tempFbo = fbos[3];
//        fbos.erase(fbos.end());
//        fbos.insert(fbos.begin(),tempFbo);
        
        fbos[0].draw(   0                      ,0  ,0.2*Mainfbo.getWidth() ,Mainfbo.getHeight()    );
        fbos[1].draw(   0.2*Mainfbo.getWidth() ,0  ,0.3*Mainfbo.getWidth() ,Mainfbo.getHeight()    );
        fbos[2].draw(   0.5*Mainfbo.getWidth() ,0  ,0.2*Mainfbo.getWidth() ,Mainfbo.getHeight()    );
        fbos[3].draw(   0.7*Mainfbo.getWidth() ,0  ,0.3*Mainfbo.getWidth() ,Mainfbo.getHeight()    );
    }
    Mainfbo.end();
    
    ofEnableDepthTest();
    
    gameCamera.begin();
	shader.begin();
    shader.setUniformTexture("tex0", Mainfbo.getTextureReference() , 1 );
    shader.setUniformTexture("maskTex", fboLust.getTextureReference() , 2 );
//    shader.setUniformTexture("tex1", fbos[fbos.size()-2].getTextureReference() , 2 );
//    shader.setUniformTexture("tex2", fbos[fbos.size()-3].getTextureReference() , 3 );
    
    int shadertime = (ofGetFrameNum()%(int)Mainfbo.getWidth());
    shader.setUniform1f("time", shadertime);
    shader.setUniform1f("limit", Mainfbo.getWidth());
    
    
    boxMesh.draw();
    shader.end();
    if (fixedObjects){
        ofPushStyle();
        ofSetLineWidth(1);
        float linecolor = abs(255-floatingGrey);
        
        ofPushMatrix();
        ofTranslate(300, 60, 220);
        ofRotate((float)((int)(ofGetFrameNum()*1)%360), (1+sin(ofGetElapsedTimef())), 0,0);
        ofSetColor(255-(int)(ofGetFrameNum()*0.1)%255,(int)(ofGetFrameNum()*0.1)%230,(int)(ofGetFrameNum()*0.1)%245);
        ofDrawCone(20, 100);
        ofNoFill();
        ofSetColor(floatingGrey);
        ofDrawCone(20, 100);
        ofFill();
        ofPopMatrix();
        
        ofPushMatrix();
    //    ofTranslate(ofGetWindowWidth()/2-50, ofGetWindowHeight()/2-50, -200);
        ofTranslate(200, 50, 420);
        ofRotate((float)((int)(ofGetFrameNum()*1)%360), (1+sin(ofGetElapsedTimef())), 0,0);
        ofSetColor((int)(ofGetFrameNum()*0.1)%230,255-(int)(ofGetFrameNum()*0.1)%245,(int)(ofGetFrameNum()*0.1)%255);
        ofDrawBox(100);
        ofNoFill();
        ofSetColor(floatingGrey);
        ofDrawBox(100);
        ofFill();
        ofPopMatrix();
        
        ofPushMatrix();
    //    ofTranslate(ofGetWindowWidth()/2-50, ofGetWindowHeight()/2-50, 500);
        ofTranslate(150, 70, 240);
        ofRotate((float)((int)(ofGetFrameNum()*1)%360), (1+sin(ofGetElapsedTimef())), 0,0);
        ofSetColor((int)((ofGetFrameNum()+2550)*0.1)%230,(int)((ofGetFrameNum()+2550)*0.1)%255,255-(int)(ofGetFrameNum()*0.1)%215);
        ofSetSphereResolution(4+(int)((ofGetFrameNum()%1000)/500));
        ofDrawSphere(32);
        ofNoFill();
        ofSetColor(floatingGrey);
        ofDrawSphere(32);
        ofFill();
        ofPopMatrix();
        
        ofPopStyle();
    }
    
    gameCamera.end();
    ofDisableDepthTest();
    
//    videograb.draw(0, 0, ofGetScreenWidth(), ofGetScreenHeight());
//    lust.draw(50,50);
    
    int vierGewinnt = ofGetFrameNum()%4;
    if (vierGewinnt == 0){
        fboSize = ofVec2f(bufferGrid.x, bufferGrid.y);
        drawFbo();
    }
    if(!fixedObjects){
        ofPushStyle();
        
        ofPushMatrix();
        ofTranslate(ofGetWindowWidth()/2-50, ofGetWindowHeight()/2-50, 100);
        ofRotate((float)((int)(ofGetFrameNum()*1)%360), (1+sin(ofGetElapsedTimef())), 0,0);
        ofSetColor(255-(int)(ofGetFrameNum()*0.1)%255,(int)(ofGetFrameNum()*0.1)%230,(int)(ofGetFrameNum()*0.1)%245);
        ofDrawCone(20, 100);
        ofNoFill();
        ofSetColor(floatingGrey);
        ofDrawCone(20, 100);
        ofFill();

        ofPopMatrix();
        
        ofPushMatrix();
        ofTranslate(ofGetWindowWidth()/2-50, ofGetWindowHeight()/2-50, 100);
        ofTranslate(150, 150);
        ofRotate((float)((int)(ofGetFrameNum()*1)%360), (1+sin(ofGetElapsedTimef())), 0,0);
        ofSetColor((int)(ofGetFrameNum()*0.1)%230,255-(int)(ofGetFrameNum()*0.1)%245,(int)(ofGetFrameNum()*0.1)%255);
        ofDrawBox(100);
        ofNoFill();
        ofSetColor(floatingGrey);
        ofDrawBox(100);
        ofFill();
        ofPopMatrix();
        
        ofPushMatrix();
        ofTranslate(ofGetWindowWidth()/2-50, ofGetWindowHeight()/2-50, 100);
        ofTranslate(-150, 100);
        ofRotate((float)((int)(ofGetFrameNum()*1)%360), (1+sin(ofGetElapsedTimef())), 0,0);
        ofSetColor((int)((ofGetFrameNum()+2550)*0.1)%230,(int)((ofGetFrameNum()+2550)*0.1)%255,255-(int)(ofGetFrameNum()*0.1)%215);
        ofSetSphereResolution(4+(int)((ofGetFrameNum()%1000)/500));
        ofDrawSphere(101);
        ofNoFill();
        ofSetColor(floatingGrey);
        ofDrawSphere(101);
        ofFill();
        ofPopMatrix();
        
        ofPopStyle();
    }
    
//    images[0].draw(0, 0);
//    lust.draw(0,0);

    ofPushStyle();
    ofSetColor(abs(255-floatingGrey)%255);
    // informative stuff
    float whereX = (((float)mouseX/(float)ofGetWindowWidth())-0.5)*2;    // -1 to 1 on window
    float whereY = (((float)mouseY/(float)ofGetWindowHeight())-0.5)*2;   // -1 to 1 on window
    ofDrawBitmapString("whereX: " + ofToString(whereX) + " whereY: " + ofToString(whereY), 20, 20);
    ofDrawBitmapString("r: load cam pos  return: save cam pos  m: tgl mouse  n: bufferGrid  u: fboSize  f: tgl fix objects  t: tgl fullscreen", 20, 40);
    ofPopStyle();
    
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    switch(key){
        case OF_KEY_UP:
//            cam.lookAt(ofVec3f(cam.getLookAtDir().x*360,cam.getLookAtDir().y*360,cam.getLookAtDir().z*360));
//            cout << "hit up. new lookAt: " <<
//            " x: "<< cam.getLookAtDir().x <<
//            " y: "<< cam.getLookAtDir().y <<
//            " z: "<< cam.getLookAtDir().z << endl;
//            cam.setGlobalPosition(110, 110, 1000);
            break;
        case OF_KEY_DOWN:
//            cam.setScale(cam.getScale()*0.5);
//            cout << "hit down. new position: " <<
//            " x: "<< cam.getGlobalPosition().x <<
//            " y: "<< cam.getGlobalPosition().y <<
//            " z: "<< cam.getGlobalPosition().z << endl;
            break;
        case 'r':
            gameCamera.loadCameraPosition();
            break;
        case OF_KEY_RETURN:
            gameCamera.saveCameraPosition();
            break;
        case 'm':
            mouseMover =! mouseMover;
            break;
        case 'n':
            bufferGrid = ofVec2f(mouseX,mouseY);
            break;
        case 'u':
            fboSize =ofVec2f(bufferGrid.x, bufferGrid.y);
            drawFbo();
            break;
        case 'f':
            fixedObjects =! fixedObjects;
            break;
        case 't':
            ofToggleFullscreen();
            break;
        default:
            cout << "hit some arbitrary key: " << key << endl;
            break;
    }

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){
    /*
     get vertical mouse position relative to window-width to look around
     */
    if(mouseMover) {
        float whereX = (((float)x/(float)ofGetWindowWidth())-0.5)*2;    // -1 to 1 on window
        float whereY = (((float)y/(float)ofGetWindowHeight())-0.5)*2;   // -1 to 1 on window
        ofDrawBitmapString(ofToString(whereX), 20, 20);
        ofQuaternion quat = gameCamera.getOrientationQuat();
        
        /*
         
         whereX and whereY on the screen:
         -1,-1  -1,0    -1,1
         
         0,-1    0,0     0,1
         
         1,-1    1,0     1,1
         
         so quaternion will be 
         
         */
        
        quat.makeRotate((abs(whereX*0.5)+abs(whereY*0.5))*180, -whereY,-whereX,0);
        
        gameCamera.setOrientation(quat);
    }
    
    /**
     try the same with relative mouse, to get a Quake 3, like first person thing, but it won't work unfortunately
     */
    //  (mDX + mDY) * x = 1
    //  x = 1 / (mDX + mDY)
    
//    int mouseDX = mouseX - mouseRX;
//    int mouseDY = mouseY - mouseRY;
//    
////    float mrXY = 1 / mouseDX + mouseDY;
//    
//    ofDrawBitmapString(ofToString(mouseDX), 20, 20);
//    ofQuaternion quat = gameCamera.getOrientationQuat();
//    
//    quat.makeRotate((abs(mouseDX*0.5)+abs(mouseDY*0.5))*180, mouseDY,mouseDX,0);
//    
//    gameCamera.setOrientation(quat);
//    
//    mouseRX = mouseX;
//    mouseRY = mouseY;
    
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}