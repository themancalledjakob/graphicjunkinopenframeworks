#pragma once

#include "ofMain.h"

#include "ofxGameCamera.h"

class testApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();
        void makeMesh(float boxHeight, float boxWidth, float boxDepth);
        void drawFbo();
        void drawLust();
        void drawSuperFbo();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    ofFbo Mainfbo;
    vector <ofFbo> fbos;
    ofFbo fboLust;
    
    ofFbo superFbo;
    
    ofVboMesh boxMesh;
    float roomWidth;
	float roomHeight;
	float roomDepth;
    
    ofShader shader;
    ofShader finalShader;
    ofxGameCamera gameCamera;
    
    ofVec2f fboSize;
    
    bool mouseMover;
    
    ofImage lust;
    ofVec2f bufferGrid;
    
    vector <ofImage> images;
    
    bool fixedObjects;
};
